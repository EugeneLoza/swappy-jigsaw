# Swappy Jigsaw

Swappy Jigsaw is a simple jigsaw puzzle game, where you have to unravel an image split into many tiles. Click to pick a tile and click another tile to swap them. There are 84 images total.

The game is made in Castle Game Engine (Lazarus/FreePascal), is free, libre and open source.

# Windows

No installation is required, just extract all files into a single folder and play.

# Linux

No installation is required, just extract all files into a single folder and play. You may need to set "executable" permission for SwappyJigsaw.run.

Libraries required for the game (Linux: Debian/Ubuntu package reference):

* libopenal1
* libpng
* zlib1g
* libvorbis
* libfreetype6
* libatk-adaptor (soft dependency of GTK2)
* You'll also need OpenGL drivers for your videocard. Usually it is libgl1-mesa-dev.
* You also need X-system and GTK at least version 2, however, you are very likely to have those already installed :)

# Links

Source code repository: https://gitlab.com/EugeneLoza/swappy-jigsaw

Please, report any bugs, issues and ideas to: https://gitlab.com/EugeneLoza/swappy-jigsaw/issues

Windows/Linux executables can be found here: https://decoherence.itch.io/swappy-jigsaw

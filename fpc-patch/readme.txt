There's some problem with image alpha-channel scaling in latest FPC versions.
If you're having non-transparent background (just white or messy) for buttons, apply this patch to Free Pascal Compiler (thanks @michaliskambi!)

It's easy to apply a patch in fpcupdeluxe. Use button "Setup +", then "Add FPC Patch", apply the patch and recompile.
